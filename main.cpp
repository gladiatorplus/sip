#include <iostream>
#ifdef __linux
#include <signal.h>
#endif
using namespace std;

#include "./sip/SipServer.h"

static void intHandler(int dummy) {
  keepRunning = 0;
}

int main() {

    #ifdef __linux
    signal(SIGINT, intHandler);
    #endif
    
    SipInfo info = {
            "41010500002000000001",
            "1234567890123456",
            "172.29.3.23",
            8116,
            "12344321",   // 作用，IPC的注册名，注册密码
            "4101050000",
            "bajiuwulian1006",
            1800,
            3600,
            10000,
            "UDP"
            };
    
    SipServer sipServer(&info);
    sipServer.loop();
    // SipClientInfo cInfo = {
    //   "41010500002000000002",
    //   8117,
    //   "172.29.3.23",
    //   "41010500002000000001",
    //   "bajiuwulian1006",
    //   8116,
    //   "4101050000",
    //   "UDP"
    // };

    // SipClient sipClient(&cInfo);
    // sipClient.loop();
}