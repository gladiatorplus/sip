
#include "SipHttpAuth.h"

void CvtHex(char *input_binary, size_t input_len, char *output_hexa) {
  unsigned short i;
  unsigned char j;

  for (i = 0; i < input_len; i++) {
    j = (input_binary[i] >> 4) & 0xf;

    if (j <= 9)
      output_hexa[i * 2] = (j + '0');

    else
      output_hexa[i * 2] = (j + 'a' - 10);

    j = input_binary[i] & 0xf;

    if (j <= 9)
      output_hexa[i * 2 + 1] = (j + '0');

    else
      output_hexa[i * 2 + 1] = (j + 'a' - 10);
  };

  output_hexa[i * 2] = '\0';
}

/* calculate H(A1) as per spec */
void DigestCalcHA1(const char *pszAlg, const char *pszUserName, const char *pszRealm, const char *pszPassword, const char *pszNonce, const char *pszCNonce, char SessionKey[MD5HEXLEN + 1]) {
  osip_MD5_CTX Md5Ctx;
  char HA1[MD5HASHLEN];
  char HA1Hex[MD5HEXLEN + 1];

  osip_MD5Init(&Md5Ctx);
  osip_MD5Update(&Md5Ctx, (unsigned char *) pszUserName, (unsigned int) strlen(pszUserName));
  osip_MD5Update(&Md5Ctx, (unsigned char *) ":", 1);
  osip_MD5Update(&Md5Ctx, (unsigned char *) pszRealm, (unsigned int) strlen(pszRealm));
  osip_MD5Update(&Md5Ctx, (unsigned char *) ":", 1);
  osip_MD5Update(&Md5Ctx, (unsigned char *) pszPassword, (unsigned int) strlen(pszPassword));
  osip_MD5Final((unsigned char *) HA1, &Md5Ctx);

  if ((pszAlg != NULL) && osip_strcasecmp(pszAlg, "md5-sess") == 0) {
    CvtHex(HA1, MD5HASHLEN, HA1Hex);
    osip_MD5Init(&Md5Ctx);
    osip_MD5Update(&Md5Ctx, (unsigned char *) HA1Hex, MD5HEXLEN);
    osip_MD5Update(&Md5Ctx, (unsigned char *) ":", 1);
    osip_MD5Update(&Md5Ctx, (unsigned char *) pszNonce, (unsigned int) strlen(pszNonce));
    osip_MD5Update(&Md5Ctx, (unsigned char *) ":", 1);
    osip_MD5Update(&Md5Ctx, (unsigned char *) pszCNonce, (unsigned int) strlen(pszCNonce));
    osip_MD5Final((unsigned char *) HA1, &Md5Ctx);
  }

  CvtHex(HA1, MD5HASHLEN, SessionKey);
}

/* calculate request-digest/response-digest as per HTTP Digest spec */
void DigestCalcResponse(char HA1[MD5HEXLEN + 1],     /* H(A1) */
                               const char *pszNonce,        /* nonce from server */
                               const char *pszNonceCount,   /* 8 hex digits */
                               const char *pszCNonce,       /* client nonce */
                               const char *pszQop,          /* qop-value: "", "auth", "auth-int" */
                               int Aka,                     /* Calculating AKAv1-MD5 response */
                               const char *pszMethod,       /* method from the request */
                               const char *pszDigestUri,    /* requested URL */
                               char HEntity[MD5HEXLEN + 1], /* H(entity body) if qop="auth-int" */
                               char Response[MD5HEXLEN + 1]
                               /* request-digest or response-digest */) {
  osip_MD5_CTX Md5Ctx;
  char HA2[MD5HASHLEN];
  char RespHash[MD5HASHLEN];
  char HA2Hex[MD5HEXLEN + 1];

  /* calculate H(A2) */
  osip_MD5Init(&Md5Ctx);
  osip_MD5Update(&Md5Ctx, (unsigned char *) pszMethod, (unsigned int) strlen(pszMethod));
  osip_MD5Update(&Md5Ctx, (unsigned char *) ":", 1);
  osip_MD5Update(&Md5Ctx, (unsigned char *) pszDigestUri, (unsigned int) strlen(pszDigestUri));

  if (pszQop == NULL) {
    goto auth_withoutqop;

  } else if (0 == osip_strcasecmp(pszQop, "auth-int")) {
    goto auth_withauth_int;

  } else if (0 == osip_strcasecmp(pszQop, "auth")) {
    goto auth_withauth;
  }

auth_withoutqop:
  osip_MD5Final((unsigned char *) HA2, &Md5Ctx);
  CvtHex(HA2, MD5HASHLEN, HA2Hex);

  /* calculate response */
  osip_MD5Init(&Md5Ctx);
  osip_MD5Update(&Md5Ctx, (unsigned char *) HA1, MD5HEXLEN);
  osip_MD5Update(&Md5Ctx, (unsigned char *) ":", 1);
  osip_MD5Update(&Md5Ctx, (unsigned char *) pszNonce, (unsigned int) strlen(pszNonce));
  osip_MD5Update(&Md5Ctx, (unsigned char *) ":", 1);

  goto end;

auth_withauth_int:

  osip_MD5Update(&Md5Ctx, (unsigned char *) ":", 1);
  osip_MD5Update(&Md5Ctx, (unsigned char *) HEntity, MD5HEXLEN);

auth_withauth:
  osip_MD5Final((unsigned char *) HA2, &Md5Ctx);
  CvtHex(HA2, MD5HASHLEN, HA2Hex);

  /* calculate response */
  osip_MD5Init(&Md5Ctx);
  osip_MD5Update(&Md5Ctx, (unsigned char *) HA1, MD5HEXLEN);
  osip_MD5Update(&Md5Ctx, (unsigned char *) ":", 1);
  osip_MD5Update(&Md5Ctx, (unsigned char *) pszNonce, (unsigned int) strlen(pszNonce));
  osip_MD5Update(&Md5Ctx, (unsigned char *) ":", 1);

  if (Aka == 0) {
    osip_MD5Update(&Md5Ctx, (unsigned char *) pszNonceCount, (unsigned int) strlen(pszNonceCount));
    osip_MD5Update(&Md5Ctx, (unsigned char *) ":", 1);
    osip_MD5Update(&Md5Ctx, (unsigned char *) pszCNonce, (unsigned int) strlen(pszCNonce));
    osip_MD5Update(&Md5Ctx, (unsigned char *) ":", 1);
    osip_MD5Update(&Md5Ctx, (unsigned char *) pszQop, (unsigned int) strlen(pszQop));
    osip_MD5Update(&Md5Ctx, (unsigned char *) ":", 1);
  }

end:
  osip_MD5Update(&Md5Ctx, (unsigned char *) HA2Hex, MD5HEXLEN);
  osip_MD5Final((unsigned char *) RespHash, &Md5Ctx);
  CvtHex(RespHash, MD5HASHLEN, Response);
}
