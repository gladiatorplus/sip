#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <osip2/osip_mt.h>
#include <eXosip2/eXosip.h>
#include <string>
#include <map>

static volatile int keepRunning = 2;
static char error_reason[1024] = {'\0'};


typedef struct SipInfo{
    std::string ua;
    std::string nonce;//服务器随机数值

    std::string ip;
    int port;
    std::string id;
    std::string realm; //服务器域
    std::string password;

    int timeout;
    int expiry;
    int rtpPort;

    std::string transport; //UDP,TCP

}SipInfo;


typedef struct Devices {
    std::string ip;
    std::string deviceID;
    int port;
    bool isReg;//是否注册
    bool isInvite;
    bool rtpPort;

    Devices() {
        ip = "";
        deviceID = "";
        port = 0;
        isInvite = false;
        isReg = false;
        rtpPort = 0;
    }

}Devices;

class SipServer{
public:
    explicit SipServer(SipInfo* info);
    virtual ~SipServer();

    void loop();

protected:
    int initSipServer();
    int sipEventHandle(eXosip_event_t *event);

    //Server
    int  responseRegister(eXosip_event_t *event);
    void responseMessage(eXosip_event_t *event);
    void responseInviteAck(eXosip_event_t *event);
    void responseMessageAnswer(eXosip_event_t *event,int code);
    void responseRegister401Unauthorized(eXosip_event_t *event);

    int requestInvite(std::string deviceID, std::string deviceIp, int devicePort);

private:
    SipInfo* mSipInfo;
    std::map<std::string,Devices> m_mapDevice;
    struct eXosip_t *mSipCtx;
    struct timeval mTimeStart;
    struct timeval mTimeEnd;
    struct timeval mTimeSub;

    //client
    int mRegId;
};