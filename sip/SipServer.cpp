#include "SipServer.h"
#include "osip2/osip_time.h"
#include "osipparser2/headers/osip_authorization.h"
#include "osipparser2/osip_parser.h"
#include "osipparser2/osip_message.h"
#include "Log.h"
#include "SipHttpAuth.h"
#include "SipTool.h"

SipServer::SipServer(SipInfo* info){
    mSipInfo = info;
}

SipServer::~SipServer(){

}

int SipServer::initSipServer(){
    mSipCtx  = eXosip_malloc();
    if (eXosip_init(mSipCtx)) {
        LOG_ERROR("eXosip_init error");
        return -1;
    }

    eXosip_set_user_agent(mSipCtx, mSipInfo->ua.c_str());
    if (eXosip_add_authentication_info(mSipCtx, mSipInfo->id.c_str(),mSipInfo->id.c_str(), mSipInfo->password.c_str(), NULL, mSipInfo->realm.c_str())){
        LOG_ERROR("eXosip_add_authentication_info error");
        eXosip_quit(mSipCtx);
        osip_free(mSipCtx);
        return -1;
    }

    osip_gettimeofday(&mTimeStart, NULL);

    int err = -1;
    if (osip_strcasecmp(mSipInfo->transport.c_str(), "UDP") == 0) {
        err = eXosip_listen_addr(mSipCtx, IPPROTO_UDP, NULL, mSipInfo->port, AF_INET, 0);

    } else if (osip_strcasecmp(mSipInfo->transport.c_str(), "TCP") == 0) {
        err = eXosip_listen_addr(mSipCtx, IPPROTO_TCP, NULL, mSipInfo->port, AF_INET, 0);

    } 
    // else if (osip_strcasecmp(mSipInfo->transport.c_str(), "TLS") == 0) {
    //     err = eXosip_listen_addr(mSipCtx, IPPROTO_TCP, NULL, mSipInfo->port, AF_INET, 1);

    // } else if (osip_strcasecmp(mSipInfo->transport.c_str(), "DTLS") == 0) {
    //     err = eXosip_listen_addr(mSipCtx, IPPROTO_UDP, NULL, mSipInfo->port, AF_INET, 1);
    // }

    if (err < 0) {
        LOG_ERROR("eXosip_listen_addr error");
        eXosip_quit(mSipCtx);
        osip_free(mSipCtx);
        return -1;
    }

    return 0;
}

void SipServer::loop() {
    if(initSipServer() != 0) {
        LOG_ERROR("Init failed!");
        return;
    }

    for(;keepRunning;) {
        eXosip_event_t *event;

        if (!(event = eXosip_event_wait(mSipCtx, 0, 20))) {
            eXosip_lock(mSipCtx);
            eXosip_automatic_action(mSipCtx);
            eXosip_unlock(mSipCtx);
            osip_usleep(10000);
            continue;
        }
        
        eXosip_automatic_action(mSipCtx);
        sipEventHandle(event);
        eXosip_event_free(event);
    }

    eXosip_quit(mSipCtx);
    osip_free(mSipCtx);
}

int SipServer::sipEventHandle(eXosip_event_t *event) {
    switch (event->type) {
    case EXOSIP_REGISTRATION_SUCCESS:
        osip_gettimeofday(&mTimeEnd, NULL);
        osip_timersub(&mTimeEnd, &mTimeStart, &mTimeSub);
        LOG_INF("REGISTRATION REPORT:[SUCCESS] [%s][duration:%li,%03lis] REGISTER [%i][%s]", mSipInfo->transport.c_str(), mTimeSub.tv_sec, mTimeSub.tv_usec / 1000, event->response->status_code, event->response->reason_phrase);
        keepRunning = 0;
        break;

    case EXOSIP_REGISTRATION_FAILURE:
        osip_gettimeofday(&mTimeEnd, NULL);
        osip_timersub(&mTimeEnd, &mTimeStart, &mTimeSub);
        
        if (event->response == NULL) {
            LOG_INF("REGISTRATION REPORT:[FAILURE] [%s][duration:%li,%03lis] REGISTER [408][       ] err=%s", mSipInfo->transport.c_str(), mTimeSub.tv_sec, mTimeSub.tv_usec / 1000, error_reason[0] == '\0' ? "no answer" : error_reason);
            keepRunning = 0;
        } 

        break;

    case EXOSIP_CALL_INVITE: {
        osip_message_t *answer;
        int i;

        i = eXosip_call_build_answer(mSipCtx, event->tid, 405, &answer);

        if (i != 0) {
        LOG_ERROR("failed to reject INVITE");
        break;
        }

        osip_free(answer->reason_phrase);
        answer->reason_phrase = osip_strdup("No Support for Incoming Calls");
        i = eXosip_call_send_answer(mSipCtx, event->tid, 405, answer);

        if (i != 0) {
        LOG_ERROR("failed to reject INVITE");
        break;
        }

        LOG_INF("INVITE rejected with 405");
        break;
        }

    case EXOSIP_MESSAGE_NEW: 
        LOG_INF("EXOSIP_MESSAGE_NEW type=%d",event->type);

        if (MSG_IS_REGISTER(event->request)) {
            responseRegister(event);
        }
        else if (MSG_IS_MESSAGE(event->request)) {
            responseMessage(event);
        }
        else if(strncmp(event->request->sip_method, "BYE", 3) != 0){
            LOG_ERROR("unknown1");
        }
        else{
            LOG_ERROR("unknown2");
        }
        break;

    case EXOSIP_IN_SUBSCRIPTION_NEW: {
      
      break;
    }

    case EXOSIP_CALL_ANSWERED: {
        if (event->response){
            int code = osip_message_get_status_code(event->response);                            
            osip_message_t* ack;
            eXosip_call_build_ack(mSipCtx,event->did,&ack);  
            eXosip_call_send_ack(mSipCtx,event->did,ack); 

            
            sdp_message_t *sdp_msg = eXosip_get_remote_sdp(mSipCtx, event->did);
            if ( !sdp_msg )
                break;

            sdp_connection_t *connection = eXosip_get_video_connection(sdp_msg);
            if ( !connection )
                break;
            char *remote_ip = connection->c_addr;

            sdp_media_t * video_sdp = eXosip_get_video_media(sdp_msg);
            if ( !video_sdp ) 
                break;
            char* remote_port = video_sdp->m_port;

            /*media_type:rtp_over_udp/rtp_over_tcp*/            
            printf("m_media:%s,m_port:%s,m_number_of_port:%s,m_proto:%s\n", \
                    video_sdp->m_media,video_sdp->m_port,video_sdp->m_number_of_port,video_sdp->m_proto);

            /*setup:active/passive*/
            char setup[64];
            for (int i = 0; i < video_sdp->a_attributes.nb_elt; i++)
            {
                sdp_attribute_t *attr = (sdp_attribute_t*)osip_list_get(&video_sdp->a_attributes, i);
                printf("%s : %s\n", attr->a_att_field, attr->a_att_value);
                if(strcmp(attr->a_att_field,"setup")==0) strcpy(setup, attr->a_att_value);
            }
        }
        break;
    }
    case EXOSIP_CALL_CLOSED:
    case EXOSIP_CALL_RELEASED:
      break;

    default:
      LOG_INF("received unknown eXosip event (type, did, cid) = (%d, %d, %d)", event->type, event->did, event->cid);
    }
}

int SipServer::responseRegister(eXosip_event_t *event) {
    osip_authorization_t * auth = nullptr;
    osip_message_get_authorization(event->request,0,&auth);

    if(auth && auth->username) {
        char *method = NULL, // REGISTER
        *algorithm = NULL, // MD5
        *username = NULL,// 41010500002000000001
        *realm = NULL, // sip服务器传给客户端，客户端携带并提交上来的sip服务域
        *nonce = NULL, //sip服务器传给客户端，客户端携带并提交上来的nonce
        *nonce_count = NULL,
        *uri = NULL; // sip:41010500002000000001@41010500002

        method = event->request->sip_method;
        char calc_response[MD5HEXLEN];
        char HA1[MD5HEXLEN + 1];
        char HA2[MD5HEXLEN + 1] = "";
        char Response[MD5HEXLEN + 1];

#define SIP_STRDUP(field) if (auth->field) (field) = osip_strdup_without_quote(auth->field)
        SIP_STRDUP(algorithm);
        SIP_STRDUP(username);
        SIP_STRDUP(realm);
        SIP_STRDUP(nonce);
        SIP_STRDUP(nonce_count);
        SIP_STRDUP(uri);

        DigestCalcHA1(algorithm, username, realm, mSipInfo->password.c_str(), nonce, nonce_count, HA1);
        DigestCalcResponse(HA1, nonce, nonce_count, auth->cnonce, auth->message_qop, 0, method, uri, HA2, Response);

        char HA3[MD5HEXLEN + 1];
        char Response1[MD5HEXLEN + 1];
        DigestCalcHA1("REGISTER", username, mSipInfo->realm.c_str(), mSipInfo->password.c_str(),  mSipInfo->nonce.c_str(), NULL, HA3);
        DigestCalcResponse(HA3, mSipInfo->nonce.c_str(), NULL, NULL, NULL, 0, method, uri, NULL, Response1);
        memcpy(calc_response, Response1, MD5HEXLEN);

        if (!memcmp(calc_response, Response, MD5HEXLEN)) {
            LOG_INF("Save register device!");
            osip_contact_t *contact = nullptr;
            osip_message_get_contact(event->request,0,&contact);
            Devices device;
            device.ip = contact->url->host;
            if(contact->url->port != "")
                device.port = atoi(contact->url->port);
            device.isReg = true;
            device.deviceID = contact->url->username;
            if(m_mapDevice.find(device.ip) != m_mapDevice.end()) {
                m_mapDevice.insert(std::pair<std::string , Devices>(device.ip,device));
            }

            responseMessageAnswer(event,200);

            LOG_INF("Send Invite!");

            // if(requestInvite(device.deviceID,device.ip,device.port) < 0) {
            //     LOG_ERROR("Request Invite Error!");
            // }
            
        } else {
            responseMessageAnswer(event,401);
            LOG_ERROR("Failed to registe");
        }

        osip_free(algorithm);
        osip_free(username);
        osip_free(realm);
        osip_free(nonce);
        osip_free(nonce_count);
        osip_free(uri);

    } else {
        //客户端信息过期，会进入 ，待处理
        responseRegister401Unauthorized(event);
    }
}

void SipServer::responseMessage(eXosip_event_t *event) {
    osip_body* body = nullptr;
    std::string deviceID;
    std::string cmdType;
    osip_message_get_body(event->request, 0, &body);

    if(body) {
        parse_xml(body->body, "<DeviceID>", false, "</DeviceID>", false, deviceID);
        parse_xml(body->body, "<CmdType>", false, "</CmdType>", false, cmdType);
        LOG_INF("cmdType = %s,deviceID = %s",cmdType.c_str(), deviceID.c_str());

    }

    if(!strcmp(cmdType.c_str(), "Catalog")) {
        responseMessageAnswer(event,200);
        // 需要根据对方的Catelog请求，做一些相应的应答请求
    }
    else if(!strcmp(cmdType.c_str(), "Keepalive")){
        responseMessageAnswer(event,200);
    }else{
        responseMessageAnswer(event,200);
    }
}

void SipServer::responseInviteAck(eXosip_event_t *event) {
    osip_message_t* msg = nullptr;
    int ret = eXosip_call_build_ack(mSipCtx, event->did, &msg);
    if(!ret && msg) {
        eXosip_call_send_ack(mSipCtx, event->did, msg);
    } else {
        LOG_ERROR("Send Invite ack failed!");
    }
}

void SipServer::responseMessageAnswer(eXosip_event_t *event,int code) {
    int ret = -1;
    osip_message_t *pReg = nullptr;

    ret = eXosip_message_build_answer(mSipCtx, event->tid, code, &pReg);
    bool bReg = pReg == nullptr ? false:true;
    if(ret == 0 && bReg) {
        eXosip_lock(mSipCtx);
        eXosip_message_send_answer(mSipCtx, event->tid,code,pReg);
        eXosip_unlock(mSipCtx);
    } else {
        LOG_ERROR("code=%d,ret=%d,bRegister=%d",code,ret,bReg);
    }
}

void SipServer::responseRegister401Unauthorized(eXosip_event_t *event) {
    char *dest = nullptr;
    osip_message_t * reg = nullptr;
    osip_www_authenticate_t * header = nullptr;

    osip_www_authenticate_init(&header);
    osip_www_authenticate_set_auth_type (header, osip_strdup("Digest"));
    osip_www_authenticate_set_realm(header,osip_enquote(mSipInfo->realm.c_str()));
    osip_www_authenticate_set_nonce(header,osip_enquote(mSipInfo->nonce.c_str()));
    osip_www_authenticate_to_str(header, &dest);
    int ret = eXosip_message_build_answer (mSipCtx, event->tid, 401, &reg);
    if ( ret == 0 && reg != nullptr ) {
        osip_message_set_www_authenticate(reg, dest);
        osip_message_set_content_type(reg, "Application/MANSCDP+xml");
        eXosip_lock(mSipCtx);
        eXosip_message_send_answer (mSipCtx, event->tid,401, reg);
        eXosip_unlock(mSipCtx);
        LOG_INF("response_register_401unauthorized success");
    }else {
        LOG_INF("response_register_401unauthorized error");
    }

    osip_www_authenticate_free(header);
    osip_free(dest);
}


int SipServer::requestInvite(std::string deviceID, std::string deviceIp, int devicePort) {
    int ret = 0;
    std::string session_exp;
    osip_message_t *msg = nullptr;
    std::string from,to,contact,head;
    char sdp[2048] = {0};

    from = "sip:" + mSipInfo->id +"@" + mSipInfo->ip + ":" + std::to_string(mSipInfo->port);
    contact = "sip:" + mSipInfo->id + "@" + mSipInfo->ip + ":" + std::to_string(mSipInfo->port);
    to = "sip:" + deviceID + "@" + deviceIp + ":" + std::to_string(devicePort);
 
    snprintf (sdp, 2048,
                "v=0\r\n"
                "o=%s 0 0 IN IP4 %s\r\n"
                "s=Play\r\n"
                "c=IN IP4 %s\r\n"
                "t=0 0\r\n"
                "m=video %d RTP/AVP 96 97 98 99\r\n"
                "a=recvonly\r\n"
                "a=rtpmap:96 PS/90000\r\n"
                "a=rtpmap:98 H264/90000\r\n"
                // "a=rtpmap:97 MPEG4/90000\r\n"
                // "a=rtpmap:99 H265/90000\r\n"
                "a=setup:actpass\r\n"          //active:客户端，passive：服务器， actpass：客户端，服务端
                , mSipInfo->id.c_str(),mSipInfo->ip.c_str(), mSipInfo->ip.c_str(),mSipInfo->rtpPort);
    ret = eXosip_call_build_initial_invite(mSipCtx, &msg, to.c_str(),from.c_str(),nullptr,nullptr);
    if(ret != 0 ) {
        LOG_ERROR("Build invite message failed!");
        return -1;
    }

    osip_message_set_body(msg, sdp, strlen(sdp));
    osip_message_set_content_type(msg,"application/sdp");
    session_exp = std::to_string(mSipInfo->expiry) + ";refresher=uac";
    osip_message_set_header(msg,"Session-Expires",session_exp.c_str());
    osip_message_set_supported(msg,"timer");

    int callId = eXosip_call_send_initial_invite(mSipCtx, msg);
    if (callId > 0) {
        LOG_INF("Send invite success: callId=%d",callId);
    }else{
        LOG_ERROR("Send invite error: callId=%d",callId);
        ret = -1;
    }

    return ret;
}

