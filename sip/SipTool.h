#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <osip2/osip_mt.h>
#include <osip2/osip.h>
#include <memory.h>
#include <syslog.h>
#include <string>


int parse_xml(const char *data, const char *s_mark, bool with_s_make, const char *e_mark, bool with_e_make, std::string dest) {
    const char* satrt = strstr( data, s_mark );
    char res[64] = "";

    if(satrt != NULL) {
        const char* end = strstr(satrt, e_mark);

        if(end != NULL){
            int s_pos = with_s_make ? 0 : strlen(s_mark);
            int e_pos = with_e_make ? strlen(e_mark) : 0;

            strncpy( res, satrt+s_pos, (end+e_pos) - (satrt+s_pos) );
            dest = res;
        }
        return 0;
    }
    return -1;
}

// static osip_list_t monitored_logs;

// static void add_log(int level, char *_log) {
//   osip_list_iterator_t it;
//   struct monitored_log *ml;

//   ml = (struct monitored_log *) osip_list_get_first(&monitored_logs, &it);

//   while (ml != OSIP_SUCCESS) {
//     if (ml != NULL && strcmp(ml->log, _log) == 0) {
//       ml->count++;
//       return;
//     }

//     ml = (struct monitored_log *) osip_list_get_next(&it);
//   }

//   ml = (struct monitored_log *) osip_malloc(sizeof(struct monitored_log));
//   ml->log_level = level;
//   ml->count = 1;
//   snprintf(ml->log, sizeof(ml->log), "%s", _log);
//   osip_list_add(&monitored_logs, ml, -1);
// }

// static void dump_logs() {
//   while (!osip_list_eol(&monitored_logs, 0)) {
//     struct monitored_log *ml = (struct monitored_log *) osip_list_get(&monitored_logs, 0);
//     syslog_wrapper(ml->log_level, "[count=%i] %s", ml->count, ml->log);
//     osip_list_remove(&monitored_logs, 0);
//     osip_free(ml);
//   }
// }