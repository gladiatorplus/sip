// #include <iostream>
// #include <sys/types.h>
// #include <sys/socket.h>
// #include <netinet/in.h>
// #include <netdb.h>
// #include <osip2/osip_mt.h>
// #include <eXosip2/eXosip.h>
// #include <string>
// #include <map>

// static volatile int keepRunning = 2;
// static char error_reason[1024] = {'\0'};

// typedef struct SipClientInfo {
//     std::string clientId;
//     int userPort;
//     std::string serverIp;
//     std::string serverId;
//     std::string password;
//     int serverPort;
//     std::string realm;
//     std::string transport;
//     std::string from;
//     std::string to;
//     std::string proxy;
//     std::string outboundproxy;
//     std::string contact;
// }SipClientInfo;


// typedef struct Device {
//     std::string ip;
//     std::string deviceID;
//     int port;
//     bool isReg;//是否注册
//     bool isInvite;
//     bool rtpPort;

//     Device() {
//         ip = "";
//         deviceID = "";
//         port = 0;
//         isInvite = false;
//         isReg = false;
//         rtpPort = 0;
//     }

// }Device;

// class SipClient{
// public:
//     explicit SipClient(SipClientInfo* info);
//     virtual ~SipClient();

//     void loop();

// protected:
//     int initSipClient();
//     int sipEventHandle(eXosip_event_t *event);

//     //Server
//     int  responseRegister(eXosip_event_t *event);
//     void responseMessage(eXosip_event_t *event);
//     void responseInviteAck(eXosip_event_t *event);
//     void responseMessageAnswer(eXosip_event_t *event,int code);
//     void responseRegister401Unauthorized(eXosip_event_t *event);

//     //client
//     int registerToServer();

//     int requestInvite(std::string deviceID, std::string deviceIp, int devicePort);
//     int sendKeepAlive(eXosip_event_t *event);

// private:
//     SipClientInfo* mSipInfo;
//     std::map<std::string,Device> m_mapDevice;
//     struct eXosip_t *mSipCtx;
//     struct timeval mTimeStart;
//     struct timeval mTimeEnd;
//     struct timeval mTimeSub;

//     //client
//     int mRegId;
// };