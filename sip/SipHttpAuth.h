#include <osipparser2/osip_md5.h>
#include <osipparser2/osip_port.h>
#include <string.h>

#define MD5HASHLEN 16
#define MD5HEXLEN 32

void DigestCalcHA1(const char *pszAlg, const char *pszUserName, const char *pszRealm, const char *pszPassword, const char *pszNonce, const char *pszCNonce, char SessionKey[MD5HEXLEN + 1]);
void DigestCalcResponse(char HA1[MD5HEXLEN + 1], const char *pszNonce, const char *pszNonceCount, const char *pszCNonce, const char *pszQop, int Aka, const char *pszMethod, const char *pszDigestUri, char HEntity[MD5HEXLEN + 1],
                               char Response[MD5HEXLEN + 1]);